(ns com.imaginary.views
 (:require
  [com.imaginary.content.text-etc :as content]
  [com.imaginary.subs :as subs]
  [re-frame.core :as rf]
  [taoensso.timbre :as timbre]))


; ;; ***********
; ;; Events
; ;; ***********
;
; (def lah-de-dah
;   {:a-thing "a value"
;    :current-page :lorem
;    :b-thing {}})
;
; (rf/reg-event-db
;  :logic/start-new
;  (fn [_db _event]
;    lah-de-dah))

;; ***********
;; Events
;; ***********

(rf/reg-event-db
 :click-tab
 (fn [db [stuff the-tab-clicked]]
   (timbre/info stuff the-tab-clicked)
   (timbre/info db)
   (assoc db :current-page the-tab-clicked)))



;; ***********
;; Views
;; ***********

(defn MainSection []
  (let [current-page @(rf/subscribe [::subs/current-page])]
    [:div {:class "tabs is-boxed is-medium"}
     [:ul
      [:li {:class
            (str (if (= current-page :hello) "is-active " "") "hello")
            :on-click #(rf/dispatch [:click-tab :hello])}
       [:a "Hello React"]]
      [:li {:class
            (str (if (= current-page :lorem) "is-active " "") "lorem")
            :on-click #(rf/dispatch [:click-tab :lorem])}
       [:a "Lorem Ipsum"]]
      [:li {:class
            (str (if (= current-page :login) "is-active " "") "login")
            :on-click #(rf/dispatch [:click-tab :login])}
       [:a "Login Form"]]
      [:li {:class
            (str (if (= current-page :fun) "is-active " "") "fun")
            :on-click #(rf/dispatch [:click-tab :fun])}
       [:a "Fun People"]]]]))


(defn PageTitle []
  [:h1 "So, here we are."])

(defn TabsApp
  "the root component"
  []
  [:section {:class "section"}
   [:div {:class "container"}
    [MainSection]
    [:section {:class "content"}
       (content/Titles)
       (content/Content)]]])
