(ns com.imaginary.subs
  (:require
    [re-frame.core :as rf]))




;; ***********
;; Events
;; ***********

(def lah-de-dah
  {:a-thing "a value"
   :current-page :hello
   :count-lorem 1
   :form-submit false
   :user-data {:username "dummy"
               :password "not dummy"}
   :fun-people {:Bill "he likes to party."
                :Billy "he parties harder than Bill."
                :William "he doesn't party at all."}
   :num-funs 3
   :b-thing {}})

(rf/reg-event-db
 :logic/start-new
 (fn [_db _event]
   lah-de-dah))

;; ***********
;; Subscriptions
;; ***********

(rf/reg-sub
 ::num-funs
 (fn [db _]
   (:num-funs db)))

(rf/reg-sub
 ::fun-list
 (fn [db _]
   (:fun-people db)))

(rf/reg-sub
 ::user-data
 (fn [db _]
   (:user-data db)))

(rf/reg-sub
 ::form-status
 (fn [db _]
   (:form-submit db)))

(rf/reg-sub
 ::current-page
 (fn [db _]
   (:current-page db)))

(rf/reg-sub
 ::count-lorem
 (fn [db _]
   (:count-lorem db)))
