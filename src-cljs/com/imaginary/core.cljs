(ns com.imaginary.core
  (:require
   [com.imaginary.views :refer [TabsApp]]
   [goog.dom :as gdom]
   [goog.functions :as gfunctions]
   [oops.core :refer [ocall]]
   [reagent.dom :as reagent-dom]
   [re-frame.core :as rf]
   [taoensso.timbre :as timbre]))

(def app-container-el (gdom/getElement "root"))

(defn on-refresh
  "Forces a Reagent re-render of all components.
   This function is called after every shadow-cljs hot module reload."
  []
  (rf/clear-subscription-cache!)
  (reagent-dom/force-update-all))

(def start-rendering!
  (gfunctions/once
   (fn []
     (timbre/info "Begin rendering 📔 ... 📔 ... 📔")
     (reagent-dom/render [(var TabsApp)] app-container-el))))

(def init!
  "Global application initialisation
   This may only be called once."
  (gfunctions/once
   (fn []
     (timbre/info "Initialising the tabs exercise 📔"
       (if-not app-container-el
         (timbre/fatal "<div id=appContainer> element not found 📔")
         (do
           (rf/dispatch-sync [:logic/start-new])
           (start-rendering!)))))))

;(defn init []
(ocall js/window "addEventListener" "load" init!)
