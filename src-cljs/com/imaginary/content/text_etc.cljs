(ns com.imaginary.content.text-etc
  (:require
   [com.imaginary.subs :as subs]
   [goog.dom :as gdom]
   [oops.core :refer [oset! oget oget+ ocall]]
   [re-frame.core :as rf]
   [taoensso.timbre :as timbre]))

;; ***********
;; Events
;; ***********
(defn LoremPara []
  [:p
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus
      faucibus at magna sit amet tristique. Suspendisse ut varius dui,
      tincidunt semper sapien. Nullam bibendum eros lectus, eu posuere
      velit ultrices sed. Proin consectetur lacus nec metus ullamcorper,
      ac tempus felis eleifend. Donec eu euismod nisl. Morbi fringilla
      justo sit amet blandit dictum. Fusce sollicitudin ipsum ut mauris
      posuere pharetra. Praesent vitae elit nec ex placerat faucibus a
      in diam. Sed bibendum sit amet dui at facilisis. Vivamus vitae
      felis lacinia, gravida lectus id, placerat ipsum. Interdum et
      malesuada fames ac ante ipsum primis in faucibus. Integer
      feugiat, sem in interdum tempor, nisi enim mollis nibh, luctus
      laoreet massa ligula nec ex."])

;(def lorem-el (gdom/getElement "content"))

(rf/reg-event-db
  :rm-onefun
  (fn [db [_ who]]
    (timbre/info (str "removing a fun person 🥩 Bye " who))
    (if-not (= (db :num-funs) 0)
      (assoc db :num-funs (- (db :num-funs) 1)))))

(rf/reg-event-db
 :add-rm
 (fn [db [_ type]]
   (if (= type :add)
     (assoc db :count-lorem (+ (db :count-lorem) 1))
     (if-not (= (db :count-lorem) 0)
        (assoc db :count-lorem (- (db :count-lorem) 1))))))

(rf/reg-event-db
 :reg-user
 (fn [db [_]]
   (timbre/info (str "777777777777777777777777  " (db :form-submit)))
   ;(-> db
   (assoc db :form-submit (not (db :form-submit)))))
     ;(assoc-in [:users :username] name)
     ;(assoc-in [:users :password] pwd))))




(defn a-thing []
  (timbre/info "This is something!"))

(defn Titles []
  (let [current-page @(rf/subscribe [::subs/current-page])
        lorem-count @(rf/subscribe [::subs/count-lorem])]
    [:h1 {:class "title"}
     (case current-page
              :hello "Hello React"
              :lorem (str "Number of Lorem Ipsum Paragraphs: " lorem-count)
              :login ""
              :fun "List of Fun People"
              "")]))

(defn Hello []
  (list
   [:p "React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes."]
   [:p "Declarative views make your code more predictable and easier to debug."]))


(defn Lorem []
  (let [lorem-count @(rf/subscribe [::subs/count-lorem])]
    (list
     [:div {:class "buttons"}
      [:button {:class "button is-info is-medium"
                :on-click #(rf/dispatch [:add-rm :add])}
       "Add Paragraph"]
      [:button {:class "button is-info is-medium"
                :on-click #(rf/dispatch [:add-rm :remove])}
       "Remove Paragraph"]]
     [:div {:class "content"
            :id "content"}
       (repeat lorem-count (LoremPara))])))

(defn Login []
  (let [{:keys [username password]} @(rf/subscribe [::subs/user-data])
        form-submit? @(rf/subscribe [::subs/form-status])]
    (timbre/info (str "username: " username " & pass: " password))
    (list
     [:div {:style {:height "40px"}}]
     [:div {:class "columns is-centered"}
      [:div {:class "columns is-half"}
       [:div {:class "box"}
        (if (not form-submit?)
          (list
           [:h1 {:class "title"} "Login"]
           [:form
            [:div {:class "field"}
             [:label {:class "label"} "Username"]
             [:div {:class "control"}
              [:input {:placeholder "Username"
                       :type "text"
                       :id "usernameInput"
                       :class "input is-medium"}]]
             [:div {:class "field"}
              [:label {:class "label"} "Password"]
              [:div {:class "control"}
               [:input {:placeholder "Password"
                        :type "password"
                        :class "input is-medium"}]]]
             [:div {:class "field"}
              [:div {:class "control"}
               [:button {:class "button is-primary is-medium"
                         :type "submit"
                         :on-click #(rf/dispatch [:reg-user])} ;"fred" "lovescars"])}

                "Login"]]]]])
          (list
           [:h1 {:class "title"} "Login Success!"]
           [:button {:class "button is-primary is-medium"
                     :on-click #(rf/dispatch [:reg-user])} ;"" ""])}
            "Reset Login Form"]))]]])))

(defn FunPeople []
  (let [funhouse? @(rf/subscribe [::subs/num-funs])
        fun-names @(rf/subscribe [::subs/fun-list])]
    (list
     [:div {:class "box"}
      [:form
       [:div {:class "field"}
        [:label {:class "label"} "Name"]
        [:div {:class "control"}
         [:input {:type "text"
                  :id "nameInput"
                  :class "input is-medium"}]]
        [:div {:class "field"}
         [:label {:class "label"} "Reason they are fun"]
         [:div {:class "control"}
          [:input {:type "text"
                   :class "input is-medium"}]]]
        [:div {:class "field"}
         [:div {:class "control"}
          [:input {:class "button is-link is-medium is-disabled"
                   :type "submit"
                   :value "Add Fun Person"
                   :disabled true}]]]]]]
     (if (> funhouse? 0)
       (list
        [:div {:class "columns"}]
        (repeat funhouse?
                (list
                 [:div {:class "column is-4"}
                  [:div {:class "box content is-medium"}
                    "a person here 🌿   |  "
                   [:button {:class "button is-link"
                             :on-click #(rf/dispatch [:rm-onefun "name"])}
                    "remove this fun person 🥩"]]])))
       [:div {:class "content is-large"}
        "Unfortunately, no one is fun 💧"]))))
                  ;:on-click #(rf/dispatch [:reg-user])}





(defn Content []
  (let [current-page @(rf/subscribe [::subs/current-page])]
    (case current-page
      :hello (Hello)
      :lorem (Lorem)
      :login (Login)
      :fun (FunPeople)
      "")))
